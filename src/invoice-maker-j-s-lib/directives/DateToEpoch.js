angular.module('invoiceMakerJsLib.directives').directive('dateToEpoch', ['$filter', function ($filter) {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function dateTimeLinker(scope, element, attrs, ngModel) {
      if (!ngModel) return;

      var type = attrs.type;

      ngModel.$formatters.push(function (modelValue) {
        var formattedString;

        if (type === 'time') {
          formattedString = $filter('date')(modelValue, 'HH:mm');
        } else if (type === 'date') {
          formattedString = $filter('date')(modelValue, 'yyyy-MM-dd');
        } else if (type === 'datetime-local') {
          formattedString = $filter('date')(modelValue, 'yyyy-MM-ddTHH:mm');
        }
        return formattedString;
      });

      ngModel.$parsers.push(function (modelValue) {
        return (new Date($filter('date')(modelValue, 'yyyy-MM-ddTHH:mm:ssZ'))).valueOf();
      });
    }
  };
}]);


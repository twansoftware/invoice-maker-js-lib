angular.module('invoiceMakerJsLib.directives').directive('stringToNumber', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function (value) {
        if (value == null || isNaN(value)) {
          return null;
        } else {
          return value.toString();
        }
      });
      ngModel.$formatters.push(function (value) {
        if (value == null || isNaN(value)) {
          return null;
        } else {
          return parseFloat(value);
        }
      });
    }
  };
});

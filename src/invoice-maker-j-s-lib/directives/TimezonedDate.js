angular.module('invoiceMakerJsLib.directives')
  .directive('timezonedDate', function () {
    return {
      priority: 1,
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, elem, attrs, ngModel) {
        var toView = function (val) {
          if (val) {
            var offset = moment(val).utcOffset();
            var date = new Date(moment(val).subtract(offset, 'm'));
            var newOffset = moment.tz.zone(attrs.timezone).offset(date);
            return new Date(moment(date).subtract(newOffset, 'm').unix() * 1000);
          }
        };

        var toModel = function (val) {
          if (val) {
            var offset = moment(val).utcOffset();
            var date = new Date(moment(val).add(offset, 'm'));
            var newOffset = moment.tz.zone(attrs.timezone).offset(date);
            return moment(date).add(newOffset, 'm').unix() * 1000;
          } else {
            return null;
          }
        };

        ngModel.$formatters.push(toView);
        ngModel.$parsers.push(toModel);
      }
    };

  });

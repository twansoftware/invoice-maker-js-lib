(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('invoiceMakerJsLib.config', [])
    .value('invoiceMakerJsLib.config', {
      debug: false
    });

  angular.module('invoiceMakerJsLib.util', []);
  // Modules
  angular.module('invoiceMakerJsLib.directives', []);
  angular.module('invoiceMakerJsLib.filters', []);
  angular.module('invoiceMakerJsLib.services', []);
  angular.module('invoiceMakerJsLib',
    [
      'invoiceMakerJsLib.config',
      'invoiceMakerJsLib.util',
      'invoiceMakerJsLib.directives',
      'invoiceMakerJsLib.filters',
      'invoiceMakerJsLib.services'
    ]);

})(angular);

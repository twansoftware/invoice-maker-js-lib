angular.module('invoiceMakerJsLib.filters').filter('invoiceMakerCurrency', ['$filter', function ($filter) {
  return function (input) {
    var userInput = String(input);
    var returnOutput = '';

    if (userInput) {
      var newSymbol;
      var args = Array.prototype.slice.call(arguments);

      var negativeValueBoolean = userInput < 0;

      var currencySymbol = args[1].currency_symbol;
      var decimalPlaces = args[1].decimal_places;
      var decimalSymbol = args[1].decimal_symbol;
      var spaceAroundSymbol = args[1].space_around_symbol;
      var symbolAtStart = args[1].symbol_at_start;
      var thousandSymbol = args[1].thousand_symbol;

      if (decimalPlaces < 0) {
        decimalPlaces = 0;
      }

      var newOutput = $filter('currency')(userInput, '$', decimalPlaces);
      newOutput = newOutput.replace('$', '');
      if (negativeValueBoolean) {
        newOutput = newOutput.replace('(', '');
        newOutput = newOutput.replace(')', '');
      }

      if (decimalPlaces != 0) {
        var periodIndex = newOutput.indexOf('.');
        var nonDecimalPart = newOutput.substr(0, periodIndex);
        var decimalPart = newOutput.substr(periodIndex);
        newOutput = nonDecimalPart.split(',').join(thousandSymbol) + decimalPart.split('.').join(decimalSymbol);
      } else {
        newOutput = newOutput.split(',').join(thousandSymbol);
      }

      if (spaceAroundSymbol === true && symbolAtStart === true) {
        newSymbol = currencySymbol + ' ';
        newOutput = newSymbol + newOutput;
      } else if (spaceAroundSymbol === true && symbolAtStart === false) {
        newSymbol = ' ' + currencySymbol;
        newOutput = newOutput + newSymbol;
      } else if (symbolAtStart === true && spaceAroundSymbol === false) {
        newSymbol = currencySymbol;
        newOutput = newSymbol + newOutput;
      } else if (symbolAtStart === false && spaceAroundSymbol === false) {
        newSymbol = currencySymbol;
        newOutput = newOutput + newSymbol;
      }

      returnOutput = newOutput;
    }

    if (negativeValueBoolean) {
      returnOutput = '(' + returnOutput + ')';
    }

    return returnOutput;
  };
}]);

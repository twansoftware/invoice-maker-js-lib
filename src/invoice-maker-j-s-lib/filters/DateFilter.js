angular.module('invoiceMakerJsLib.filters')
  .filter('invoiceMakerDate', ['invoiceutil', function (invoiceutil) {
    return function (input, timezone, date_format_option) {
      return invoiceutil.formatDateRegular(timezone, date_format_option, input);
    };
  }])
  .filter('invoiceMakerFeedDate', ['invoiceutil', function (invoiceutil) {
    return function (input, timezone, date_format_option) {
      var momentTimeZone = invoiceutil.getJsTimeZone(timezone);
      var momentLocale = invoiceutil.getJsLocale(date_format_option);
      var formatDate = moment.unix(input / 1000);
      return formatDate.tz(momentTimeZone).locale(momentLocale).calendar();
    };
  }])
  .filter('invoiceMakerMonth', ['invoiceutil', function (invoiceutil) {
    return function (input, timezone, date_format_option) {
      var momentTimeZone = invoiceutil.getJsTimeZone(timezone);
      var momentLocale = invoiceutil.getJsLocale(date_format_option);
      var formatDate = moment().month(input);
      return formatDate.tz(momentTimeZone).locale(momentLocale).format('MMM');
    };
  }]);

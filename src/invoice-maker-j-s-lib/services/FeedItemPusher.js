angular.module('invoiceMakerJsLib.services').factory('FeedItemPush', ['fireutil', function (fireutil) {
  var ROOT_FIREBASE = fireutil.getRootFirebase();

  function feedPush(feedItem) {
    if (feedItem) {
      return ROOT_FIREBASE.child('company_feeds').child(feedItem['company_id']).push(feedItem);
    }
  }

  function documentCreated(companyId, documentId, documentDisplayId, documentType, createdBy) {
    var feedItem = {};
    feedItem['type'] = 'DOCUMENT_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = documentId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'document_display_id': documentDisplayId,
      'document_type': documentType,
      'created_by': createdBy
    };
    return feedPush(feedItem);
  }

  function paymentRecorded(companyId, invoiceId, invoiceDisplayId, paymentId, amount, recordedBy) {
    var feedItem = {};
    feedItem['type'] = 'PAYMENT_RECORDED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = invoiceId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'payment_id': paymentId,
      'invoice_display_id': invoiceDisplayId,
      'amount': amount,
      'recorded_by': recordedBy

    };
    return feedPush(feedItem);
  }

  function recurringInvoiceCreated(companyId, recurringInvoiceId, recurringInvoiceDisplayId, createdBy) {
    var feedItem = {};
    feedItem['type'] = 'RECURRING_PROFILE_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = recurringInvoiceId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'recurring_invoice_display_id': recurringInvoiceDisplayId,
      'created_by': createdBy
    };
    return feedPush(feedItem);
  }

  function paymentDeleted(companyId, invoiceId, invoiceDisplayId, paymentId, amount) {
    var feedItem = {};
    feedItem['type'] = 'PAYMENT_DELETED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = invoiceId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'payment_id': paymentId,
      'amount': amount,
      'invoice_display_id': invoiceDisplayId
    };
    return feedPush(feedItem);
  }

  function documentDeleted(companyId, documentId, documentDisplayId, documentType, deletedBy) {
    var feedItem = {};
    feedItem['type'] = 'DOCUMENT_DELETED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = documentId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'document_display_id': documentDisplayId,
      'document_type': documentType,
      'deleted_by': deletedBy
    };
    return feedPush(feedItem);
  }

  function clientCreated(companyId, clientId, clientName, createdBy) {
    var feedItem = {};
    feedItem['type'] = 'CLIENT_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = clientId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'client_name': clientName,
      'created_by': createdBy
    };
    return feedPush(feedItem);
  }

  function clientDeleted(companyId, clientId, clientName, deletedBy) {
    var feedItem = {};
    feedItem['type'] = 'CLIENT_DELETED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = clientId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'client_name': clientName,
      'deleted_by': deletedBy
    };
    return feedPush(feedItem);
  }

  function productCreated(companyId, productId, productName, createdBy) {
    var feedItem = {};
    feedItem['type'] = 'PRODUCT_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = productId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'product_name': productName,
      'created_by': createdBy
    };
    return feedPush(feedItem);
  }

  function productDeleted(companyId, productId, productName, deletedBy) {
    var feedItem = {};
    feedItem['type'] = 'PRODUCT_DELETED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = productId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'product_name': productName,
      'deleted_by': deletedBy
    };
    return feedPush(feedItem);
  }

  function inventoryAcquired(companyId, productId, productName, quantityAcquired, costPerItem, recordedBy) {
    var feedItem = {};
    feedItem['type'] = 'PRODUCT_INVENTORY_ACQUIRED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = productId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'product_name': productName,
      'quantity_acquired': quantityAcquired,
      'cost_per_item': costPerItem,
      'recorded_by': recordedBy
    };
    return feedPush(feedItem);
  }

  function companyCreated(companyId, companyName) {
    var feedItem = {};
    feedItem['type'] = 'COMPANY_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'name': companyName
    };
    return feedPush(feedItem);
  }

  function estimateConvertedToInvoice(companyId, invoiceId, invoiceDisplayId, convertedBy) {
    var feedItem = {};
    feedItem['type'] = 'ESTIMATE_CONVERTED_TO_INVOICE';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = invoiceId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'converted_by': convertedBy,
      'invoice_display_id': invoiceDisplayId,
    };
    return feedPush(feedItem);
  }

  function inviteAccepted(companyId, inviteAcceptedBy) {
    var feedItem = {};
    feedItem['type'] = 'USER_ACCEPTED_INVITE';
    feedItem['company_id'] = companyId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'accepting_user': inviteAcceptedBy
    };
    return feedPush(feedItem);
  }

  return {
    documentCreated: documentCreated,
    paymentRecorded: paymentRecorded,
    documentDeleted: documentDeleted,
    paymentDeleted: paymentDeleted,
    recurringInvoiceCreated: recurringInvoiceCreated,
    clientCreated: clientCreated,
    clientDeleted: clientDeleted,
    productCreated: productCreated,
    productDeleted: productDeleted,
    inventoryAcquired: inventoryAcquired,
    estimateConvertedToInvoice: estimateConvertedToInvoice,
    companyCreated: companyCreated,
    inviteAccepted: inviteAccepted
  };

}]);

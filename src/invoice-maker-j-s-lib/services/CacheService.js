angular.module('invoiceMakerJsLib.services').service('CacheService', ['fireutil', function (fireutil) {
  var self = this;
  var ROOT_FIREBASE = fireutil.getRootFirebase();

  var bigCache = {};

  self.init = function (companyId) {
    var clientNameCache = {};
    var productCache = {};
    var documentIdCache = {};
    var recurringCache = {};

    bigCache[companyId] = {
      'clients': clientNameCache,
      'products': productCache,
      'documentIds': documentIdCache,
      'recurring': recurringCache
    };

    var clientsFirebase = ROOT_FIREBASE.child('clients').child(companyId);
    var documentsFirebase = ROOT_FIREBASE.child('invoices').child(companyId);
    var recurringInvoicesFirebase = ROOT_FIREBASE.child('recurring_invoices').child(companyId);
    var productsFirebase = ROOT_FIREBASE.child('products').child(companyId);

    var clientFunction = function (snapshot) {
      clientNameCache[snapshot.key] = snapshot.child('name').val();
    };

    var documentFunction = function (snapshot) {
      documentIdCache[snapshot.key] = snapshot.child('display_id').val();
    };

    var productFunction = function (snapshot) {
      productCache[snapshot.key] = snapshot.val();
    };

    var recurringFunction = function (snapshot) {
      recurringInvoicesFirebase[snapshot.key] = snapshot.val();
    };

    clientsFirebase.on('child_added', clientFunction);
    clientsFirebase.on('child_changed', clientFunction);

    documentsFirebase.on('child_added', documentFunction);
    documentsFirebase.on('child_changed', documentFunction);

    productsFirebase.on('child_added', productFunction);
    productsFirebase.on('child_changed', productFunction);

    recurringInvoicesFirebase.on('child_added', recurringFunction);
    recurringInvoicesFirebase.on('child_changed', recurringFunction);
  };

  self.getCachedClientName = function (companyId, clientId) {
    return bigCache[companyId]['clients'][clientId];
  };

  self.getCachedDocumentId = function (companyId, documentId) {
    return bigCache[companyId]['documentIds'][documentId];
  };

  self.getCachedProducts = function (companyId) {
    return bigCache[companyId]['products'];
  };
}]);

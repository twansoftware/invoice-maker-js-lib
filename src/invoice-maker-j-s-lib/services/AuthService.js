angular.module('invoiceMakerJsLib.services').factory('AuthService', ['fireutil', function (fireutil) {
  var firebaseAuth = fireutil.getFirebaseAuth();

  return {
    authedWithUserPass: function () {
      return firebaseAuth.$getAuth() !== null && firebaseAuth.$getAuth().isAnonymous === false;
    },
    authedAnonymously: function () {
      return firebaseAuth.$getAuth() !== null && firebaseAuth.$getAuth().isAnonymous;
    },
    authAnonymously: function () {
      return firebaseAuth.$signInAnonymously();
    },
    loginWithUsernamePassword: function (username, password) {
      return firebaseAuth.$signInWithEmailAndPassword(username || "", password || "");
    },
    loginWithPopup: function (provider) {
      return firebaseAuth.$signInWithPopup(provider);
    },
    logout: function () {
      return firebaseAuth.$signOut();
    },
    register: function (email, password) {
      return firebaseAuth.$createUserWithEmailAndPassword(email, password);
    },
    resetPassword: function (email) {
      return firebaseAuth.$sendPasswordResetEmail(email);
    },
    getUserId: function () {
      return firebaseAuth.$getAuth().uid;
    },
    getUserEmail: function () {
      return firebaseAuth.$getAuth().email;
    },
    getUserToken: function () {
      return firebaseAuth.$getAuth().token;
    },
    changePassword: function (email, password, newPassword) {
      return firebaseAuth.$changePassword({
        email: email,
        oldPassword: password,
        newPassword: newPassword
      });
    }
  }

}]);

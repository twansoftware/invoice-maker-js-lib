angular.module('invoiceMakerJsLib.services').factory('CalculationQueuePush', ['fireutil', function (fireutil) {
  var ROOT_FIREBASE = fireutil.getRootFirebase();

  function calcPush(calcJob) {
    if (calcJob) {
      ROOT_FIREBASE.child('calculation_queue').push(calcJob);
    }
  }

  function sampleDocumentGeneration(entityId, isSetupData) {
    var calcJob = {};
    calcJob['entity_id'] = entityId;
    calcJob['extra_string_id'] = String(isSetupData);
    calcJob['job_type'] = 'SAMPLE_DOCUMENT_GENERATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function documentCalculationUpdate(companyId, documentId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = documentId;
    calcJob['job_type'] = 'DOCUMENT_CALCULATION';
    calcJob['extra_string_id'] = String(true);
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function refundPayment(companyId, invoiceId, paymentId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = invoiceId;
    calcJob['extra_string_id'] = paymentId;
    calcJob['job_type'] = 'REFUND_PAYMENT';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processUserRegistration(userId, userEmail, deviceType) {
    var calcJob = {};
    calcJob['user_id'] = userId;
    calcJob['user_email'] = userEmail;
    calcJob['extra_string_id'] = deviceType;
    calcJob['job_type'] = 'PROCESS_USER_REGISTRATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function acceptInvite(userId, userEmail, desiredCompanyId) {
    var calcJob = {};
    calcJob['user_id'] = userId;
    calcJob['user_email'] = userEmail;
    calcJob['extra_string_id'] = desiredCompanyId;
    calcJob['job_type'] = 'ACCEPT_INVITE';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processStripeIap(userId, ccToken, subscriptionTerm, userEmail) {
    var calcJob = {};
    calcJob['job_type'] = 'PROCESS_STRIPE_SUBSCRIPTION';
    calcJob['user_id'] = userId;
    calcJob['extra_string_id'] = ccToken;
    calcJob['extra_extra_string_id'] = subscriptionTerm;
    calcJob['extra_extra_extra_string_id'] = userEmail;
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function documentGenerationUpdate(companyId, documentId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = documentId;
    calcJob['job_type'] = 'DOCUMENT_GENERATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function regenerateClientDocuments(companyId, clientId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = clientId;
    calcJob['job_type'] = 'REGENERATE_CLIENT_DOCUMENTS';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function setupStripe(companyId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['job_type'] = 'SETUP_NEW_STRIPE';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function regenerateAllDocuments(companyId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['job_type'] = 'REGENERATE_ALL_DOCUMENTS';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function chargeCard(companyId, chargeRequestId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['job_type'] = 'CHARGE_CARD';
    calcJob['entity_id'] = chargeRequestId;
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function productCalculationUpdate(companyId, documentId, invoiceItemId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = invoiceItemId;
    calcJob['extra_string_id'] = documentId;
    calcJob['job_type'] = 'PRODUCT_DEDUCTION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function productDeletion(companyId, productId, documentId, deletedInvoiceItemId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = productId;
    calcJob['extra_string_id'] = documentId;
    calcJob['extra_extra_string_id'] = deletedInvoiceItemId;
    calcJob['job_type'] = 'PRODUCT_DELETION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function productAcquisition(companyId, productId, acquisitionId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = productId;
    calcJob['extra_string_id'] = acquisitionId;
    calcJob['job_type'] = 'PRODUCT_ACQUISITION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function sendDocument(companyId, documentId, sendHistoryId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = documentId;
    calcJob['extra_string_id'] = sendHistoryId;
    calcJob['job_type'] = 'IMPROVED_DOCUMENT_SEND';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function newPortalSend(companyId, documentId, sendHistoryId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = documentId;
    calcJob['extra_string_id'] = sendHistoryId;
    calcJob['job_type'] = 'NEW_PORTAL_DOCUMENT_SEND';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function checkUserIap(userId) {
    var calcJob = {};
    calcJob['entity_id'] = userId;
    calcJob['job_type'] = 'USER_IAP';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processPushRegistration(userId, pushToken) {
    var calcJob = {};
    calcJob['user_id'] = userId;
    calcJob['extra_string_id'] = pushToken;
    calcJob['job_type'] = 'PROCESS_FCM_REGISTRATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function removePushRegistration(userId, pushToken) {
    var calcJob = {};
    calcJob['user_id'] = userId;
    calcJob['extra_string_id'] = pushToken;
    calcJob['job_type'] = 'REMOVE_FCM_REGISTRATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processDataExport(companyId, exportId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = exportId;
    calcJob['job_type'] = 'PROCESS_DATA_EXPORT';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function inviteUser(companyId, userEmail) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['extra_string_id'] = userEmail;
    calcJob['job_type'] = 'INVITE_USER';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function recurringCalculationUpdate(companyId, recurringInvoiceId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = recurringInvoiceId;
    calcJob['job_type'] = 'RECURRING_INVOICE_CALCULATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function recurringGenerationUpdate(companyId, recurringInvoiceId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = recurringInvoiceId;
    calcJob['job_type'] = 'RECURRING_INVOICE_GENERATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function scheduleRecurringInvoice(companyId, recurringInvoiceId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = recurringInvoiceId;
    calcJob['job_type'] = 'SCHEDULE_RECURRING_INVOICE';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processTaxDeletion(companyId, taxId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = taxId;
    calcJob['job_type'] = 'PROCESS_TAX_DELETION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function newPortalStatementSend(companyId, clientId, sendHistoryId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = sendHistoryId;
    calcJob['extra_string_id'] = clientId;
    calcJob['job_type'] = 'SEND_STATEMENT';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  return {
    sendPortalStatement: newPortalStatementSend,
    checkUserIap: checkUserIap,
    sampleDocumentGeneration: sampleDocumentGeneration,
    documentCalculationUpdate: documentCalculationUpdate,
    documentGenerationUpdate: documentGenerationUpdate,
    productCalculationUpdate: productCalculationUpdate,
    productDeletion: productDeletion,
    regenerateClientDocuments: regenerateClientDocuments,
    regenerateAllDocuments: regenerateAllDocuments,
    productAcquisition: productAcquisition,
    improvedSendDocument: sendDocument,
    newPortalSend: newPortalSend,
    setupStripe: setupStripe,
    processFcmRegistration: processPushRegistration,
    removeFcmRegistration: removePushRegistration,
    processStripeIap: processStripeIap,
    inviteUser: inviteUser,
    refundPayment: refundPayment,
    recurringCalculationUpdate: recurringCalculationUpdate,
    scheduleRecurringInvoice: scheduleRecurringInvoice,
    recurringGenerationUpdate: recurringGenerationUpdate,
    acceptInvite: acceptInvite,
    processUserRegistration: processUserRegistration,
    processTaxDeletion: processTaxDeletion,
    processDataExport: processDataExport
  };

}]);

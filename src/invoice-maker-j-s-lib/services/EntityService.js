angular.module('invoiceMakerJsLib.services').factory('EntityService', ['fireutil', function (fireutil) {
  var ROOT_FIREBASE = fireutil.getRootFirebase();

  return {
    getUserObject: function (userId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('users').child(userId));
    },
    getUserPushSettingsObject: function (userId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('users').child(userId).child('push_settings'));
    },
    getCompanyObject: function (companyId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('companies').child(companyId));
    },
    getCompanyChildObject: function (companyId, childName) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('companies').child(companyId).child(childName));
    },
    getClientList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('clients').child(companyId).orderByChild('deleted').equalTo(false));
    },
    getRecentFeedItemsList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('company_feeds').child(companyId).orderByChild('creation_epoch'));
    },
    getSingleEmailTemplateObject: function (companyId, templateName) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('companies').child(companyId).child('email_settings').child(templateName));
    },
    getClientObject: function (companyId, clientId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('clients').child(companyId).child(clientId));
    },
    getProductList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('products').child(companyId).orderByChild('deleted').equalTo(false));
    },
    getProductObject: function (companyId, productId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('products').child(companyId).child(productId));
    },
    getProductFigures: function (companyId, productId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('product_figures').child(companyId).child(productId).child('figures'));
    },
    getProductTransactions: function (companyId, productId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('product_figures').child(companyId).child(productId).child('transactions').orderByChild('deleted').equalTo(false));
    },
    getProductAcquisitions: function (companyId, productId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('product_acquisitions').child(companyId).child(productId));
    },
    getDocumentIdRef: function (documentId, companyId) {
      return ROOT_FIREBASE.child('invoices').child(companyId).child(documentId).child('display_id');
    },
    getCompanyFiguresObject: function (companyId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('company_figures').child(companyId).child('numbers'));
    },
    getCompanyHomeGraphsObject: function (companyId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('graphs').child(companyId).child('home'));
    },
    getEstimateList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('deleted_type').equalTo('false_ESTIMATE'));
    },
    getInvoiceList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('deleted_type').equalTo('false_INVOICE'));
    },
    getRecentInvoiceList: function (companyId, since) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('creation_date_epoch').startAt(since));
    },
    getUnpaidInvoiceList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('deleted_type_unpaid').equalTo('false_INVOICE_true'));
    },
    getClientDocumentList: function (companyId, clientId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('deleted_clientid').equalTo("false_" + clientId));
    },
    getDocumentItemList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoice_items').child(companyId).child(documentId).orderByChild('order'));
    },
    getDocumentItemObject: function (companyId, documentId, documentItemId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('invoice_items').child(companyId).child(documentId).child(documentItemId));
    },
    getDocumentObject: function (companyId, documentId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('invoices').child(companyId).child(documentId));
    },
    getDocumentSendHistoryObject: function (companyId, documentId, sendHistoryId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('send_histories').child(companyId).child(documentId).child(sendHistoryId));
    },
    getDocumentPaymentsList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('payments').child(companyId).child(documentId));
    },
    getDocumentAttachmentsList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('attachments').child(companyId).child(documentId));
    },
    getDocumentNotesList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoice_notes').child(companyId).child(documentId).orderByChild('deleted').equalTo(false));
    },
    getDocumentSendHistoryList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('send_histories').child(companyId).child(documentId).orderByChild('time_sent'));
    },
    getStatementSendHistoryList: function (companyId, clientId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('statement_send_histories').child(companyId).child(clientId).orderByChild('time_sent'));
    },
    getAccountingYear: function (companyId, year) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('graphs').child(companyId).child('fiscal_years').child(typeof year == 'string' ? year : year.toString()));
    },
    getNewCompanySetupFirebase: function (userId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('simple_setups').child(userId));
    },
    getUserCompanyInvites: function (sanitizedEmail) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invites').child(sanitizedEmail).orderByChild('accepted').equalTo(false));
    },
    getRecurringInvoiceObject: function (companyId, recurringInvoiceId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('recurring_invoices').child(companyId).child(recurringInvoiceId));
    },
    getRecurringInvoiceItemsList: function (companyId, recurringInvoiceId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('recurring_invoice_items').child(companyId).child(recurringInvoiceId).orderByChild('order'));
    },
    getRecurringInvoiceItemObject: function (companyId, recurringInvoiceId, recurringInvoiceItemId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('recurring_invoice_items').child(companyId).child(recurringInvoiceId).child(recurringInvoiceItemId));
    },
    getRecurringInvoiceList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('recurring_invoices').child(companyId).orderByChild('deleted').equalTo(false));
    },
    getRecurringNotesList: function (companyId, recurringInvoiceId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('recurring_invoice_notes').child(companyId).child(recurringInvoiceId).orderByChild('deleted').equalTo(false));
    },
    getInvoiceListForRecurringInvoice: function (companyId, recurringInvoiceId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('recurring_invoice_id').equalTo(recurringInvoiceId));
    },
    getCompanyTaxObject: function (companyId, taxKey) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('companies').child(companyId).child('company_taxes').child(taxKey));
    },
    getCompanyTaxesList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('companies').child(companyId).child('company_taxes').orderByChild('deleted').equalTo(false));
    },
    getCompanyExportsList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('exports').child(companyId));
    },
    getExpenseList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('receipts').child(companyId).orderByChild('deleted').equalTo(false));
    },
    flattenCallbacks: function (callbacksObject, funxtion, fields) {
      fields.forEach(function (fieldName) {
        if (!callbacksObject.hasOwnProperty(fieldName)) {
          callbacksObject[fieldName] = [];
        }
        callbacksObject[fieldName].push(funxtion);
      });
    },
    saveData: function (updatedFieldName, firebaseObject, fieldUpdateCallbacks) {
      var updates = {};
      updates[updatedFieldName] = firebaseObject[updatedFieldName];
      var firebaseRef = firebaseObject.$ref();
      var callbacks = (fieldUpdateCallbacks[""] || []).concat(fieldUpdateCallbacks[updatedFieldName] || []);
      firebaseRef.update(updates, function (error) {
        if (!error && callbacks) {
          callbacks.forEach(function (callback) {
            eval(callback)();
          });
        }
      });
    }
  };
}]);

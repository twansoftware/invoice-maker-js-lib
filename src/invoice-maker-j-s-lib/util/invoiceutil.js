angular.module('invoiceMakerJsLib.util').service('invoiceutil', [function () {
  var US_CURRENCY = {
    currency_symbol: "$",
    space_around_symbol: false,
    symbol_at_start: true,
    thousand_symbol: ",",
    decimal_symbol: ".",
    decimal_places: "2"
  };

  var UK_CURRENCY = {
    currency_symbol: "£",
    space_around_symbol: false,
    symbol_at_start: true,
    thousand_symbol: ",",
    decimal_symbol: ".",
    decimal_places: "2"
  };

  var EUROPE_CURRENCY = {
    currency_symbol: "€",
    space_around_symbol: false,
    symbol_at_start: true,
    thousand_symbol: ",",
    decimal_symbol: ".",
    decimal_places: "2"
  };

  this.suggestSubdomain = function (companyName) {
    var MAX_COMPANY_CHARS = 30;

    var legalCompanyCharacters = [];
    var code, i, len;

    for (i = 0, len = companyName.length; i < len && legalCompanyCharacters.length < MAX_COMPANY_CHARS; i++) {
      code = companyName.charCodeAt(i);
      if ((code > 47 && code < 58) || // numeric (0-9)
        (code > 64 && code < 91) || // upper alpha (A-Z)
        (code > 96 && code < 123)) { // lower alpha (a-z)
        legalCompanyCharacters.push(companyName.charAt(i).toLowerCase());
      }
    }
    return legalCompanyCharacters.join("");
  };

  this.isTaxRateValid = function (invoice, rate) {
    var invoiceRate = invoice[rate];
    return this.isTaxRateValidInternal(invoiceRate);
  };

  this.formatDateRegular = function (timezone, date_format_option, input) {
    var momentTimeZone = this.getJsTimeZone(timezone);
    var momentLocale = this.getJsLocale(date_format_option);
    var formatDate = moment.unix(input / 1000);
    return formatDate.tz(momentTimeZone).locale(momentLocale).format('LL');
  };

  this.isTaxRateValidInternal = function (invoiceRate) {
    return invoiceRate != null && invoiceRate.tax_name != null && invoiceRate.tax_rate != null && invoiceRate.tax_name.length > 0 && invoiceRate.tax_rate.length > 0;
  };

  this.calculateTaxAmount = function (item, invoice, taxRate) {
    if (!this.isTaxRateValidInternal(taxRate) || !invoice.apply_taxes) {
      return 0;
    }
    if ((item.apply_tax_1 && taxRate == invoice.tax_one) || (item.apply_tax_2 && taxRate == invoice.tax_two)) {
      var itemAmount = this.calculateRealItemRate(item) * Number(item.quantity) || 0;
      if (taxRate.compounded && taxRate == invoice.tax_two) {
        itemAmount = itemAmount + this.calculateTaxAmount(item, invoice, invoice.tax_one);
      }
      var taxRateString = taxRate.tax_rate;
      var taxRateDecimal = Number(taxRateString) / 100;
      return itemAmount * taxRateDecimal;
    } else {
      return 0;
    }
  };

  this.makePdfFilename = function (document) {
    var prefix = document.type == "INVOICE" ? "INV" : "EST";
    var displayId = document.display_id;
    return prefix + displayId + ".pdf";
  };

  this.makeSubject = function (company, document) {
    var isInvoice = document.type == "INVOICE";
    var subjectTemplateName = isInvoice ? "NEW_INVOICE" : "NEW_ESTIMATE";
    var defaultMessage = isInvoice ? "New invoice ::invoice number:: from ::company name::" : "New estimate ::estimate number:: from ::company name::";
    var subject = company.email_settings == null || company.email_settings[subjectTemplateName] == null || !company.email_settings[subjectTemplateName]
      ? defaultMessage : company.email_settings[subjectTemplateName]['subject'];
    return this.subVariables(subject, company, document);
  };

  this.subVariables = function (templateString, company, document) {
    return templateString.replace("::invoice number::", document.display_id)
      .replace("::estimate number::", document.display_id)
      .replace("::company name::", company.company_information.company_name);
  };

  this.countryCurrenciesMap = {
    "US": ["USD"],
    "CA": ["CAD", "USD"],
    "GB": ["GBP", "EUR", "USD"],
    "AU": ["AUD"],
    "BR": ["BRL"],
    "SE": ["SEK", "NOK", "DKK", "EUR", "GBP", "USD"],
    "NO": ["NOK", "DKK", "SEK", "EUR", "GBP", "USD"],
    "FI": ["EUR", "NOK", "SEK", "DKK", "GBP", "USD"],
    "IE": ["EUR", "GBP", "USD"],
    "DK": ["DKK", "NOK", "SEK", "EUR", "GBP", "USD"],
    "AT": ["EUR", "GBP", "USD"],
    "BE": ["EUR", "GBP", "USD"],
    "FR": ["EUR", "GBP", "USD"],
    "DE": ["EUR", "GBP", "USD"],
    "IT": ["EUR", "GBP", "USD"],
    "LU": ["EUR", "GBP", "USD"],
    "NL": ["EUR", "GBP", "USD"],
    "ES": ["EUR", "GBP", "USD"]
  };

  this.quarterlyDataShouldBeShown = function (quarterOne, quarterTwo, quarterThree, quarterFour) {
    function quarterHasData(singleQuarter) {
      for (var key in singleQuarter) {
        if (key == 'month_one_revenue' || key == 'month_two_revenue' || key == 'month_three_revenue') {
          if (singleQuarter.hasOwnProperty(key) && parseFloat(singleQuarter[key]) != 0) {
            return true;
          }
        }
      }
      return false;
    }

    return quarterHasData(quarterOne) || quarterHasData(quarterTwo) || quarterHasData(quarterThree) || quarterHasData(quarterFour);
  };

  this.createFullCompanyData = function (simpleSetupData, userId) {
    var returnCompany = {};
    var companyInformation = {};
    var invoiceSettings = {};
    var invoiceTemplate = {};
    returnCompany['company_information'] = companyInformation;
    returnCompany['invoice_settings'] = invoiceSettings;
    returnCompany['invoice_template'] = invoiceTemplate;

    companyInformation['company_name'] = simpleSetupData['step_one']['company_name'];
    companyInformation['contact_email'] = simpleSetupData['step_one']['company_email'];

    if ('step_two' in simpleSetupData) {
      companyInformation['address_line_one'] = simpleSetupData['step_two']['address_line_one'] || null;
      companyInformation['address_line_two'] = simpleSetupData['step_two']['address_line_two'] || null;
      companyInformation['address_line_three'] = simpleSetupData['step_two']['address_line_three'] || null;

      companyInformation['contact_fax'] = simpleSetupData['step_two']['fax'] || null;
      companyInformation['contact_phone'] = simpleSetupData['step_two']['phone'] || null;
      companyInformation['contact_business_number'] = simpleSetupData['step_two']['business_number'] || null;
    }

    var selectedCurrency = simpleSetupData['step_one']['simple_currency'];
    var currencyConfiguration;
    var timezone;
    var dateFormatOption;

    if (selectedCurrency == null || "USD" == selectedCurrency || "OTHER" == selectedCurrency) {
      currencyConfiguration = US_CURRENCY;
      dateFormatOption = "US";
      timezone = "AMERICA_NY";
    } else if ("CAD" == selectedCurrency) {
      currencyConfiguration = US_CURRENCY;
      dateFormatOption = "CANADA";
      timezone = "AMERICA_NY";
    } else if ("AUD" == selectedCurrency) {
      currencyConfiguration = US_CURRENCY;
      dateFormatOption = "AUSTRALIA";
      timezone = "AUSTRALIA_SYDNEY";
    } else if ("EUR" == selectedCurrency) {
      currencyConfiguration = EUROPE_CURRENCY;
      dateFormatOption = "UK";
      timezone = "EUROPE_BERLIN";
    } else if ("GBP" == selectedCurrency) {
      currencyConfiguration = UK_CURRENCY;
      dateFormatOption = "UK";
      timezone = "EUROPE_LONDON";
    }

    returnCompany['currency_configuration'] = currencyConfiguration;
    currencyConfiguration['timezone'] = timezone;
    currencyConfiguration['fiscal_year_start_month'] = 'JANUARY';
    invoiceSettings['date_format_setting'] = dateFormatOption;

    if ('step_three' in simpleSetupData) {
      var stepThreeData = simpleSetupData['step_three'];
      if ('logo_url' in stepThreeData) {
        invoiceTemplate['logo_url'] = stepThreeData['logo_url'];
      }
      if ('template_name' in stepThreeData) {
        invoiceTemplate['template_name'] = stepThreeData['template_name'];
      } else {
        invoiceTemplate['template_name'] = 'Modern';
      }
      if ('template_color' in stepThreeData) {
        invoiceTemplate['template_html_color'] = stepThreeData['template_color'];
      } else {
        invoiceTemplate['template_html_color'] = '#000000';
      }
    } else {
      invoiceTemplate['template_html_color'] = '#000000';
      invoiceTemplate['template_name'] = 'Modern';
    }

    invoiceSettings['next_invoice_id'] = 1;
    invoiceSettings['show_quantity_rate_columns'] = true;
    invoiceSettings['use_inventory'] = true;

    returnCompany['authorized_uids'] = {};
    returnCompany['authorized_uids'][userId] = true;
    returnCompany['creation_date'] = (new Date).getTime();
    returnCompany['owner_user_id'] = userId;

    return returnCompany;
  };

  this.displayTax = function (rate) {
    if (rate && rate.tax_name && rate.tax_rate) {
      return rate.tax_name + " (" + rate.tax_rate + "%)";
    } else {
      return "";
    }
  };

  this.displayCompanyTax = function (taxFirebaseKey, company, rateAtEpoch) {
    var tax = company.company_taxes[taxFirebaseKey];
    var currentTaxRate = this.getCurrentTaxRate(tax, rateAtEpoch);
    return tax.name + " (" + currentTaxRate.rate + "%)";
  };

  this.getCurrentTaxRate = function (tax, rateAtEpoch) {
    var winningRate = null;
    for (var key in tax.tax_rates) {
      if (tax.tax_rates.hasOwnProperty(key)) {
        var singleRate = tax.tax_rates[key];
        var effectiveEpoch = singleRate.effective_epoch;
        if (winningRate == null || (effectiveEpoch < rateAtEpoch && effectiveEpoch >= winningRate.effective_epoch)) {
          winningRate = singleRate;
        }
      }
    }
    return winningRate;
  };

  this.describeProducts = function (selectedProductsMap) {
    if (!selectedProductsMap) {
      return "";
    } else {
      var first = true;
      var returnString = "";
      for (var key in selectedProductsMap) {
        if (selectedProductsMap.hasOwnProperty(key)) {
          if (first) {
            first = false;
          } else {
            returnString = returnString + ", ";
          }
          returnString = returnString + selectedProductsMap[key].title;
        }
      }
      return returnString;
    }
  };

  this.getErrorDescriptionForError = function (error) {
    switch (error.code) {
      case "INVALID_USER":
        return "This user does not exist";
      case "EMAIL_TAKEN":
        return "Email address already in use";
      case "INVALID_PASSWORD":
        return "Incorrect password";
      case "INVALID_EMAIL":
        return "Invalid email address";
      default:
        return error.message;
    }
  };

  this.calculateRealItemRate = function (item) {
    var regularRate = item['rate'] || item['price_per_item'];
    var discountAmount = item['discount_amount'] || item['discount_per_item'];
    if (regularRate) {
      var invoiceDiscountOption = item['invoice_discount_option'] || item['discount_option'];
      var discountType = item['discount_type'];
      if (invoiceDiscountOption == 'DISCOUNT_ITEMS' && discountAmount && discountType) {
        if (discountType == 'DOLLAR') {
          return regularRate - discountAmount;
        } else {
          return regularRate - (regularRate * (discountAmount / 100));
        }
      } else {
        return regularRate;
      }
    } else {
      return 0;
    }
  };

  this.getJsTimeZone = function (invoiceTimeZone) {
    if (invoiceTimeZone == 'AMERICA_NY') {
      return 'America/New_York';
    } else if (invoiceTimeZone == 'AMERICA_CHICAGO') {
      return 'America/Chicago';
    } else if (invoiceTimeZone == 'AMERICA_PHOENIX') {
      return 'America/Phoenix';
    } else if (invoiceTimeZone == 'AMERICA_LA') {
      return 'America/Los_Angeles';
    } else if (invoiceTimeZone == 'EUROPE_LONDON') {
      return 'Europe/London';
    } else if (invoiceTimeZone == 'EUROPE_BERLIN') {
      return 'Europe/Berlin';
    } else if (invoiceTimeZone == 'EUROPE_MOSCOW') {
      return 'Europe/Moscow';
    } else if (invoiceTimeZone == 'AUSTRALIA_SYDNEY') {
      return 'Australia/Sydney';
    } else if (invoiceTimeZone == 'BRAZIL_EAST') {
      return 'Brazil/East';
    } else if (invoiceTimeZone == 'EUROPE_ROME') {
      return 'Europe/Rome';
    } else if (invoiceTimeZone == 'EUROPE_PARIS') {
      return 'Europe/Paris';
    } else if (invoiceTimeZone == 'AFRICA_JOHANNESBURG') {
      return 'Africa/Johannesburg';
    } else {
      return 'America/New_York'
    }
  };

  this.getCurrentFiscalYear = function (jsTimeZone, fiscalStartMonth) {
    var now = moment().tz(jsTimeZone);
    var currentYear = now.year();
    var currentMonth = now.month();
    var nextYear = false;

    if (!fiscalStartMonth || fiscalStartMonth == "JANUARY") {
      nextYear = false;
    } else if (fiscalStartMonth == "FEBRUARY") {
      nextYear = currentMonth >= 1;
    } else if (fiscalStartMonth == "MARCH") {
      nextYear = currentMonth >= 2;
    } else if (fiscalStartMonth == "APRIL") {
      nextYear = currentMonth >= 3;
    } else if (fiscalStartMonth == "MAY") {
      nextYear = currentMonth >= 4;
    } else if (fiscalStartMonth == "JUNE") {
      nextYear = currentMonth >= 5;
    } else if (fiscalStartMonth == "JULY") {
      nextYear = currentMonth >= 6;
    } else if (fiscalStartMonth == "AUGUST") {
      nextYear = currentMonth >= 6;
    } else if (fiscalStartMonth == "SEPTEMBER") {
      nextYear = currentMonth >= 7;
    } else if (fiscalStartMonth == "OCTOBER") {
      nextYear = currentMonth >= 8;
    } else if (fiscalStartMonth == "NOVEMBER") {
      nextYear = currentMonth >= 9;
    } else if (fiscalStartMonth == "DECEMBER") {
      nextYear = currentMonth >= 10;
    }
    return nextYear ? currentYear + 1 : currentYear;
  };

  this.getTimezoneOffset = function (invoiceTimeZone, dateEpoch) {
    return moment.unix(dateEpoch).tz(this.getJsTimeZone(invoiceTimeZone)).format('Z');
  };

  this.getJsLocale = function (invoiceLocale) {
    if (invoiceLocale == 'US') {
      return 'en';
    } else if (invoiceLocale == 'UK') {
      return 'en-gb';
    } else if (invoiceLocale == 'GERMANY') {
      return 'de';
    } else if (invoiceLocale == 'NORWAY') {
      return 'nb';
    } else if (invoiceLocale == 'ITALY') {
      return 'it';
    } else if (invoiceLocale == 'FRANCE') {
      return 'fr';
    } else if (invoiceLocale == 'JAPAN') {
      return 'ja';
    } else if (invoiceLocale == 'CHINA') {
      return 'zh-cn';
    } else if (invoiceLocale == 'CANADA') {
      return 'en-ca';
    } else if (invoiceLocale == 'AUSTRALIA') {
      return 'en-au';
    } else if (invoiceLocale == 'SOUTH_AFRICA') {
      return 'en';
    } else if (invoiceLocale == 'NETHERLANDS') {
      return 'nl';
    } else if (invoiceLocale == 'RUSSIA') {
      return 'ru';
    } else if (invoiceLocale == 'IRELAND') {
      return 'en';
    } else if (invoiceLocale == 'SPANISH') {
      return 'es';
    } else if (invoiceLocale == 'BRAZIL') {
      return 'pt-br';
    } else {
      return 'en';
    }
  }
}])
;

angular.module('invoiceMakerJsLib.util').factory('fireutil', ['$firebaseAuth', '$firebaseObject', '$firebaseArray',
  function ($firebaseAuth, $firebaseObject, $firebaseArray) {
    return {
      getFirebaseStorage: function () {
        return firebase.storage().ref();
      },
      getRootFirebase: function () {
        return firebase.database().ref();
      },
      getFirebaseAuth: function () {
        return $firebaseAuth();
      },
      getSynchronizedObject: function (firebaseRef) {
        return $firebaseObject(firebaseRef);
      },
      getSynchronizedArray: function (firebaseRef) {
        return $firebaseArray(firebaseRef);
      }
    };
  }]);

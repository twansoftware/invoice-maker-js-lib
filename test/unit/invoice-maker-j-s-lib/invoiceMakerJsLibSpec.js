'use strict';

describe('', function() {

  var module;
  var dependencies;
  dependencies = [];

  var hasModule = function(module) {
  return dependencies.indexOf(module) >= 0;
  };

  beforeEach(function() {

  // Get module
  module = angular.module('invoiceMakerJsLib');
  dependencies = module.requires;
  });

  it('should load config module', function() {
    expect(hasModule('invoiceMakerJsLib.config')).to.be.ok;
  });

  
  it('should load filters module', function() {
    expect(hasModule('invoiceMakerJsLib.filters')).to.be.ok;
  });
  

  
  it('should load directives module', function() {
    expect(hasModule('invoiceMakerJsLib.directives')).to.be.ok;
  });
  

  
  it('should load services module', function() {
    expect(hasModule('invoiceMakerJsLib.services')).to.be.ok;
  });
  

});
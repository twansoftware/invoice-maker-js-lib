(function (angular) {

  // Create all modules and define dependencies to make sure they exist
  // and are loaded in the correct order to satisfy dependency injection
  // before all nested files are concatenated by Gulp

  // Config
  angular.module('invoiceMakerJsLib.config', [])
    .value('invoiceMakerJsLib.config', {
      debug: false
    });

  angular.module('invoiceMakerJsLib.util', []);
  // Modules
  angular.module('invoiceMakerJsLib.directives', []);
  angular.module('invoiceMakerJsLib.filters', []);
  angular.module('invoiceMakerJsLib.services', []);
  angular.module('invoiceMakerJsLib',
    [
      'invoiceMakerJsLib.config',
      'invoiceMakerJsLib.util',
      'invoiceMakerJsLib.directives',
      'invoiceMakerJsLib.filters',
      'invoiceMakerJsLib.services'
    ]);

})(angular);

angular.module('invoiceMakerJsLib.directives').directive('dateToEpoch', ['$filter', function ($filter) {
  return {
    restrict: 'A',
    require: '?ngModel',
    link: function dateTimeLinker(scope, element, attrs, ngModel) {
      if (!ngModel) return;

      var type = attrs.type;

      ngModel.$formatters.push(function (modelValue) {
        var formattedString;

        if (type === 'time') {
          formattedString = $filter('date')(modelValue, 'HH:mm');
        } else if (type === 'date') {
          formattedString = $filter('date')(modelValue, 'yyyy-MM-dd');
        } else if (type === 'datetime-local') {
          formattedString = $filter('date')(modelValue, 'yyyy-MM-ddTHH:mm');
        }
        return formattedString;
      });

      ngModel.$parsers.push(function (modelValue) {
        return (new Date($filter('date')(modelValue, 'yyyy-MM-ddTHH:mm:ssZ'))).valueOf();
      });
    }
  };
}]);


angular.module('invoiceMakerJsLib.directives').directive('stringToNumber', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function (value) {
        if (value == null || isNaN(value)) {
          return null;
        } else {
          return value.toString();
        }
      });
      ngModel.$formatters.push(function (value) {
        if (value == null || isNaN(value)) {
          return null;
        } else {
          return parseFloat(value);
        }
      });
    }
  };
});

angular.module('invoiceMakerJsLib.directives')
  .directive('timezonedDate', function () {
    return {
      priority: 1,
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, elem, attrs, ngModel) {
        var toView = function (val) {
          if (val) {
            var offset = moment(val).utcOffset();
            var date = new Date(moment(val).subtract(offset, 'm'));
            var newOffset = moment.tz.zone(attrs.timezone).offset(date);
            return new Date(moment(date).subtract(newOffset, 'm').unix() * 1000);
          }
        };

        var toModel = function (val) {
          if (val) {
            var offset = moment(val).utcOffset();
            var date = new Date(moment(val).add(offset, 'm'));
            var newOffset = moment.tz.zone(attrs.timezone).offset(date);
            return moment(date).add(newOffset, 'm').unix() * 1000;
          } else {
            return null;
          }
        };

        ngModel.$formatters.push(toView);
        ngModel.$parsers.push(toModel);
      }
    };

  });

angular.module('invoiceMakerJsLib.filters')
  .filter('invoiceMakerDate', ['invoiceutil', function (invoiceutil) {
    return function (input, timezone, date_format_option) {
      return invoiceutil.formatDateRegular(timezone, date_format_option, input);
    };
  }])
  .filter('invoiceMakerFeedDate', ['invoiceutil', function (invoiceutil) {
    return function (input, timezone, date_format_option) {
      var momentTimeZone = invoiceutil.getJsTimeZone(timezone);
      var momentLocale = invoiceutil.getJsLocale(date_format_option);
      var formatDate = moment.unix(input / 1000);
      return formatDate.tz(momentTimeZone).locale(momentLocale).calendar();
    };
  }])
  .filter('invoiceMakerMonth', ['invoiceutil', function (invoiceutil) {
    return function (input, timezone, date_format_option) {
      var momentTimeZone = invoiceutil.getJsTimeZone(timezone);
      var momentLocale = invoiceutil.getJsLocale(date_format_option);
      var formatDate = moment().month(input);
      return formatDate.tz(momentTimeZone).locale(momentLocale).format('MMM');
    };
  }]);

angular.module('invoiceMakerJsLib.filters').filter('invoiceMakerCurrency', ['$filter', function ($filter) {
  return function (input) {
    var userInput = String(input);
    var returnOutput = '';

    if (userInput) {
      var newSymbol;
      var args = Array.prototype.slice.call(arguments);

      var negativeValueBoolean = userInput < 0;

      var currencySymbol = args[1].currency_symbol;
      var decimalPlaces = args[1].decimal_places;
      var decimalSymbol = args[1].decimal_symbol;
      var spaceAroundSymbol = args[1].space_around_symbol;
      var symbolAtStart = args[1].symbol_at_start;
      var thousandSymbol = args[1].thousand_symbol;

      if (decimalPlaces < 0) {
        decimalPlaces = 0;
      }

      var newOutput = $filter('currency')(userInput, '$', decimalPlaces);
      newOutput = newOutput.replace('$', '');
      if (negativeValueBoolean) {
        newOutput = newOutput.replace('(', '');
        newOutput = newOutput.replace(')', '');
      }

      if (decimalPlaces != 0) {
        var periodIndex = newOutput.indexOf('.');
        var nonDecimalPart = newOutput.substr(0, periodIndex);
        var decimalPart = newOutput.substr(periodIndex);
        newOutput = nonDecimalPart.split(',').join(thousandSymbol) + decimalPart.split('.').join(decimalSymbol);
      } else {
        newOutput = newOutput.split(',').join(thousandSymbol);
      }

      if (spaceAroundSymbol === true && symbolAtStart === true) {
        newSymbol = currencySymbol + ' ';
        newOutput = newSymbol + newOutput;
      } else if (spaceAroundSymbol === true && symbolAtStart === false) {
        newSymbol = ' ' + currencySymbol;
        newOutput = newOutput + newSymbol;
      } else if (symbolAtStart === true && spaceAroundSymbol === false) {
        newSymbol = currencySymbol;
        newOutput = newSymbol + newOutput;
      } else if (symbolAtStart === false && spaceAroundSymbol === false) {
        newSymbol = currencySymbol;
        newOutput = newOutput + newSymbol;
      }

      returnOutput = newOutput;
    }

    if (negativeValueBoolean) {
      returnOutput = '(' + returnOutput + ')';
    }

    return returnOutput;
  };
}]);

angular.module('invoiceMakerJsLib.services').factory('AuthService', ['fireutil', function (fireutil) {
  var firebaseAuth = fireutil.getFirebaseAuth();

  return {
    authedWithUserPass: function () {
      return firebaseAuth.$getAuth() !== null && firebaseAuth.$getAuth().isAnonymous === false;
    },
    authedAnonymously: function () {
      return firebaseAuth.$getAuth() !== null && firebaseAuth.$getAuth().isAnonymous;
    },
    authAnonymously: function () {
      return firebaseAuth.$signInAnonymously();
    },
    loginWithUsernamePassword: function (username, password) {
      return firebaseAuth.$signInWithEmailAndPassword(username || "", password || "");
    },
    loginWithPopup: function (provider) {
      return firebaseAuth.$signInWithPopup(provider);
    },
    logout: function () {
      return firebaseAuth.$signOut();
    },
    register: function (email, password) {
      return firebaseAuth.$createUserWithEmailAndPassword(email, password);
    },
    resetPassword: function (email) {
      return firebaseAuth.$sendPasswordResetEmail(email);
    },
    getUserId: function () {
      return firebaseAuth.$getAuth().uid;
    },
    getUserEmail: function () {
      return firebaseAuth.$getAuth().email;
    },
    getUserToken: function () {
      return firebaseAuth.$getAuth().token;
    },
    changePassword: function (email, password, newPassword) {
      return firebaseAuth.$changePassword({
        email: email,
        oldPassword: password,
        newPassword: newPassword
      });
    }
  }

}]);

angular.module('invoiceMakerJsLib.services').service('CacheService', ['fireutil', function (fireutil) {
  var self = this;
  var ROOT_FIREBASE = fireutil.getRootFirebase();

  var bigCache = {};

  self.init = function (companyId) {
    var clientNameCache = {};
    var productCache = {};
    var documentIdCache = {};
    var recurringCache = {};

    bigCache[companyId] = {
      'clients': clientNameCache,
      'products': productCache,
      'documentIds': documentIdCache,
      'recurring': recurringCache
    };

    var clientsFirebase = ROOT_FIREBASE.child('clients').child(companyId);
    var documentsFirebase = ROOT_FIREBASE.child('invoices').child(companyId);
    var recurringInvoicesFirebase = ROOT_FIREBASE.child('recurring_invoices').child(companyId);
    var productsFirebase = ROOT_FIREBASE.child('products').child(companyId);

    var clientFunction = function (snapshot) {
      clientNameCache[snapshot.key] = snapshot.child('name').val();
    };

    var documentFunction = function (snapshot) {
      documentIdCache[snapshot.key] = snapshot.child('display_id').val();
    };

    var productFunction = function (snapshot) {
      productCache[snapshot.key] = snapshot.val();
    };

    var recurringFunction = function (snapshot) {
      recurringInvoicesFirebase[snapshot.key] = snapshot.val();
    };

    clientsFirebase.on('child_added', clientFunction);
    clientsFirebase.on('child_changed', clientFunction);

    documentsFirebase.on('child_added', documentFunction);
    documentsFirebase.on('child_changed', documentFunction);

    productsFirebase.on('child_added', productFunction);
    productsFirebase.on('child_changed', productFunction);

    recurringInvoicesFirebase.on('child_added', recurringFunction);
    recurringInvoicesFirebase.on('child_changed', recurringFunction);
  };

  self.getCachedClientName = function (companyId, clientId) {
    return bigCache[companyId]['clients'][clientId];
  };

  self.getCachedDocumentId = function (companyId, documentId) {
    return bigCache[companyId]['documentIds'][documentId];
  };

  self.getCachedProducts = function (companyId) {
    return bigCache[companyId]['products'];
  };
}]);

angular.module('invoiceMakerJsLib.services').factory('CalculationQueuePush', ['fireutil', function (fireutil) {
  var ROOT_FIREBASE = fireutil.getRootFirebase();

  function calcPush(calcJob) {
    if (calcJob) {
      ROOT_FIREBASE.child('calculation_queue').push(calcJob);
    }
  }

  function sampleDocumentGeneration(entityId, isSetupData) {
    var calcJob = {};
    calcJob['entity_id'] = entityId;
    calcJob['extra_string_id'] = String(isSetupData);
    calcJob['job_type'] = 'SAMPLE_DOCUMENT_GENERATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function documentCalculationUpdate(companyId, documentId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = documentId;
    calcJob['job_type'] = 'DOCUMENT_CALCULATION';
    calcJob['extra_string_id'] = String(true);
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function refundPayment(companyId, invoiceId, paymentId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = invoiceId;
    calcJob['extra_string_id'] = paymentId;
    calcJob['job_type'] = 'REFUND_PAYMENT';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processUserRegistration(userId, userEmail, deviceType) {
    var calcJob = {};
    calcJob['user_id'] = userId;
    calcJob['user_email'] = userEmail;
    calcJob['extra_string_id'] = deviceType;
    calcJob['job_type'] = 'PROCESS_USER_REGISTRATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function acceptInvite(userId, userEmail, desiredCompanyId) {
    var calcJob = {};
    calcJob['user_id'] = userId;
    calcJob['user_email'] = userEmail;
    calcJob['extra_string_id'] = desiredCompanyId;
    calcJob['job_type'] = 'ACCEPT_INVITE';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processStripeIap(userId, ccToken, subscriptionTerm, userEmail) {
    var calcJob = {};
    calcJob['job_type'] = 'PROCESS_STRIPE_SUBSCRIPTION';
    calcJob['user_id'] = userId;
    calcJob['extra_string_id'] = ccToken;
    calcJob['extra_extra_string_id'] = subscriptionTerm;
    calcJob['extra_extra_extra_string_id'] = userEmail;
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function documentGenerationUpdate(companyId, documentId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = documentId;
    calcJob['job_type'] = 'DOCUMENT_GENERATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function regenerateClientDocuments(companyId, clientId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = clientId;
    calcJob['job_type'] = 'REGENERATE_CLIENT_DOCUMENTS';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function setupStripe(companyId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['job_type'] = 'SETUP_NEW_STRIPE';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function regenerateAllDocuments(companyId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['job_type'] = 'REGENERATE_ALL_DOCUMENTS';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function chargeCard(companyId, chargeRequestId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['job_type'] = 'CHARGE_CARD';
    calcJob['entity_id'] = chargeRequestId;
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function productCalculationUpdate(companyId, documentId, invoiceItemId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = invoiceItemId;
    calcJob['extra_string_id'] = documentId;
    calcJob['job_type'] = 'PRODUCT_DEDUCTION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function productDeletion(companyId, productId, documentId, deletedInvoiceItemId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = productId;
    calcJob['extra_string_id'] = documentId;
    calcJob['extra_extra_string_id'] = deletedInvoiceItemId;
    calcJob['job_type'] = 'PRODUCT_DELETION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function productAcquisition(companyId, productId, acquisitionId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = productId;
    calcJob['extra_string_id'] = acquisitionId;
    calcJob['job_type'] = 'PRODUCT_ACQUISITION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function sendDocument(companyId, documentId, sendHistoryId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = documentId;
    calcJob['extra_string_id'] = sendHistoryId;
    calcJob['job_type'] = 'IMPROVED_DOCUMENT_SEND';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function newPortalSend(companyId, documentId, sendHistoryId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = documentId;
    calcJob['extra_string_id'] = sendHistoryId;
    calcJob['job_type'] = 'NEW_PORTAL_DOCUMENT_SEND';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function checkUserIap(userId) {
    var calcJob = {};
    calcJob['entity_id'] = userId;
    calcJob['job_type'] = 'USER_IAP';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processPushRegistration(userId, pushToken) {
    var calcJob = {};
    calcJob['user_id'] = userId;
    calcJob['extra_string_id'] = pushToken;
    calcJob['job_type'] = 'PROCESS_FCM_REGISTRATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function removePushRegistration(userId, pushToken) {
    var calcJob = {};
    calcJob['user_id'] = userId;
    calcJob['extra_string_id'] = pushToken;
    calcJob['job_type'] = 'REMOVE_FCM_REGISTRATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processDataExport(companyId, exportId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = exportId;
    calcJob['job_type'] = 'PROCESS_DATA_EXPORT';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function inviteUser(companyId, userEmail) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['extra_string_id'] = userEmail;
    calcJob['job_type'] = 'INVITE_USER';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function recurringCalculationUpdate(companyId, recurringInvoiceId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = recurringInvoiceId;
    calcJob['job_type'] = 'RECURRING_INVOICE_CALCULATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function recurringGenerationUpdate(companyId, recurringInvoiceId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = recurringInvoiceId;
    calcJob['job_type'] = 'RECURRING_INVOICE_GENERATION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function scheduleRecurringInvoice(companyId, recurringInvoiceId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = recurringInvoiceId;
    calcJob['job_type'] = 'SCHEDULE_RECURRING_INVOICE';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function processTaxDeletion(companyId, taxId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = taxId;
    calcJob['job_type'] = 'PROCESS_TAX_DELETION';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  function newPortalStatementSend(companyId, clientId, sendHistoryId) {
    var calcJob = {};
    calcJob['company_id'] = companyId;
    calcJob['entity_id'] = sendHistoryId;
    calcJob['extra_string_id'] = clientId;
    calcJob['job_type'] = 'SEND_STATEMENT';
    calcJob['processing'] = false;
    return calcPush(calcJob);
  }

  return {
    sendPortalStatement: newPortalStatementSend,
    checkUserIap: checkUserIap,
    sampleDocumentGeneration: sampleDocumentGeneration,
    documentCalculationUpdate: documentCalculationUpdate,
    documentGenerationUpdate: documentGenerationUpdate,
    productCalculationUpdate: productCalculationUpdate,
    productDeletion: productDeletion,
    regenerateClientDocuments: regenerateClientDocuments,
    regenerateAllDocuments: regenerateAllDocuments,
    productAcquisition: productAcquisition,
    improvedSendDocument: sendDocument,
    newPortalSend: newPortalSend,
    setupStripe: setupStripe,
    processFcmRegistration: processPushRegistration,
    removeFcmRegistration: removePushRegistration,
    processStripeIap: processStripeIap,
    inviteUser: inviteUser,
    refundPayment: refundPayment,
    recurringCalculationUpdate: recurringCalculationUpdate,
    scheduleRecurringInvoice: scheduleRecurringInvoice,
    recurringGenerationUpdate: recurringGenerationUpdate,
    acceptInvite: acceptInvite,
    processUserRegistration: processUserRegistration,
    processTaxDeletion: processTaxDeletion,
    processDataExport: processDataExport
  };

}]);

angular.module('invoiceMakerJsLib.services').factory('EntityService', ['fireutil', function (fireutil) {
  var ROOT_FIREBASE = fireutil.getRootFirebase();

  return {
    getUserObject: function (userId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('users').child(userId));
    },
    getUserPushSettingsObject: function (userId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('users').child(userId).child('push_settings'));
    },
    getCompanyObject: function (companyId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('companies').child(companyId));
    },
    getCompanyChildObject: function (companyId, childName) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('companies').child(companyId).child(childName));
    },
    getClientList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('clients').child(companyId).orderByChild('deleted').equalTo(false));
    },
    getRecentFeedItemsList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('company_feeds').child(companyId).orderByChild('creation_epoch'));
    },
    getSingleEmailTemplateObject: function (companyId, templateName) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('companies').child(companyId).child('email_settings').child(templateName));
    },
    getClientObject: function (companyId, clientId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('clients').child(companyId).child(clientId));
    },
    getProductList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('products').child(companyId).orderByChild('deleted').equalTo(false));
    },
    getProductObject: function (companyId, productId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('products').child(companyId).child(productId));
    },
    getProductFigures: function (companyId, productId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('product_figures').child(companyId).child(productId).child('figures'));
    },
    getProductTransactions: function (companyId, productId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('product_figures').child(companyId).child(productId).child('transactions').orderByChild('deleted').equalTo(false));
    },
    getProductAcquisitions: function (companyId, productId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('product_acquisitions').child(companyId).child(productId));
    },
    getDocumentIdRef: function (documentId, companyId) {
      return ROOT_FIREBASE.child('invoices').child(companyId).child(documentId).child('display_id');
    },
    getCompanyFiguresObject: function (companyId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('company_figures').child(companyId).child('numbers'));
    },
    getCompanyHomeGraphsObject: function (companyId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('graphs').child(companyId).child('home'));
    },
    getEstimateList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('deleted_type').equalTo('false_ESTIMATE'));
    },
    getInvoiceList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('deleted_type').equalTo('false_INVOICE'));
    },
    getRecentInvoiceList: function (companyId, since) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('creation_date_epoch').startAt(since));
    },
    getUnpaidInvoiceList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('deleted_type_unpaid').equalTo('false_INVOICE_true'));
    },
    getClientDocumentList: function (companyId, clientId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('deleted_clientid').equalTo("false_" + clientId));
    },
    getDocumentItemList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoice_items').child(companyId).child(documentId).orderByChild('order'));
    },
    getDocumentItemObject: function (companyId, documentId, documentItemId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('invoice_items').child(companyId).child(documentId).child(documentItemId));
    },
    getDocumentObject: function (companyId, documentId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('invoices').child(companyId).child(documentId));
    },
    getDocumentSendHistoryObject: function (companyId, documentId, sendHistoryId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('send_histories').child(companyId).child(documentId).child(sendHistoryId));
    },
    getDocumentPaymentsList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('payments').child(companyId).child(documentId));
    },
    getDocumentAttachmentsList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('attachments').child(companyId).child(documentId));
    },
    getDocumentNotesList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoice_notes').child(companyId).child(documentId).orderByChild('deleted').equalTo(false));
    },
    getDocumentSendHistoryList: function (companyId, documentId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('send_histories').child(companyId).child(documentId).orderByChild('time_sent'));
    },
    getStatementSendHistoryList: function (companyId, clientId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('statement_send_histories').child(companyId).child(clientId).orderByChild('time_sent'));
    },
    getAccountingYear: function (companyId, year) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('graphs').child(companyId).child('fiscal_years').child(typeof year == 'string' ? year : year.toString()));
    },
    getNewCompanySetupFirebase: function (userId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('simple_setups').child(userId));
    },
    getUserCompanyInvites: function (sanitizedEmail) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invites').child(sanitizedEmail).orderByChild('accepted').equalTo(false));
    },
    getRecurringInvoiceObject: function (companyId, recurringInvoiceId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('recurring_invoices').child(companyId).child(recurringInvoiceId));
    },
    getRecurringInvoiceItemsList: function (companyId, recurringInvoiceId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('recurring_invoice_items').child(companyId).child(recurringInvoiceId).orderByChild('order'));
    },
    getRecurringInvoiceItemObject: function (companyId, recurringInvoiceId, recurringInvoiceItemId) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('recurring_invoice_items').child(companyId).child(recurringInvoiceId).child(recurringInvoiceItemId));
    },
    getRecurringInvoiceList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('recurring_invoices').child(companyId).orderByChild('deleted').equalTo(false));
    },
    getRecurringNotesList: function (companyId, recurringInvoiceId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('recurring_invoice_notes').child(companyId).child(recurringInvoiceId).orderByChild('deleted').equalTo(false));
    },
    getInvoiceListForRecurringInvoice: function (companyId, recurringInvoiceId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('invoices').child(companyId).orderByChild('recurring_invoice_id').equalTo(recurringInvoiceId));
    },
    getCompanyTaxObject: function (companyId, taxKey) {
      return fireutil.getSynchronizedObject(ROOT_FIREBASE.child('companies').child(companyId).child('company_taxes').child(taxKey));
    },
    getCompanyTaxesList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('companies').child(companyId).child('company_taxes').orderByChild('deleted').equalTo(false));
    },
    getCompanyExportsList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('exports').child(companyId));
    },
    getExpenseList: function (companyId) {
      return fireutil.getSynchronizedArray(ROOT_FIREBASE.child('receipts').child(companyId).orderByChild('deleted').equalTo(false));
    },
    flattenCallbacks: function (callbacksObject, funxtion, fields) {
      fields.forEach(function (fieldName) {
        if (!callbacksObject.hasOwnProperty(fieldName)) {
          callbacksObject[fieldName] = [];
        }
        callbacksObject[fieldName].push(funxtion);
      });
    },
    saveData: function (updatedFieldName, firebaseObject, fieldUpdateCallbacks) {
      var updates = {};
      updates[updatedFieldName] = firebaseObject[updatedFieldName];
      var firebaseRef = firebaseObject.$ref();
      var callbacks = (fieldUpdateCallbacks[""] || []).concat(fieldUpdateCallbacks[updatedFieldName] || []);
      firebaseRef.update(updates, function (error) {
        if (!error && callbacks) {
          callbacks.forEach(function (callback) {
            eval(callback)();
          });
        }
      });
    }
  };
}]);

angular.module('invoiceMakerJsLib.services').factory('FeedItemPush', ['fireutil', function (fireutil) {
  var ROOT_FIREBASE = fireutil.getRootFirebase();

  function feedPush(feedItem) {
    if (feedItem) {
      return ROOT_FIREBASE.child('company_feeds').child(feedItem['company_id']).push(feedItem);
    }
  }

  function documentCreated(companyId, documentId, documentDisplayId, documentType, createdBy) {
    var feedItem = {};
    feedItem['type'] = 'DOCUMENT_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = documentId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'document_display_id': documentDisplayId,
      'document_type': documentType,
      'created_by': createdBy
    };
    return feedPush(feedItem);
  }

  function paymentRecorded(companyId, invoiceId, invoiceDisplayId, paymentId, amount, recordedBy) {
    var feedItem = {};
    feedItem['type'] = 'PAYMENT_RECORDED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = invoiceId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'payment_id': paymentId,
      'invoice_display_id': invoiceDisplayId,
      'amount': amount,
      'recorded_by': recordedBy

    };
    return feedPush(feedItem);
  }

  function recurringInvoiceCreated(companyId, recurringInvoiceId, recurringInvoiceDisplayId, createdBy) {
    var feedItem = {};
    feedItem['type'] = 'RECURRING_PROFILE_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = recurringInvoiceId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'recurring_invoice_display_id': recurringInvoiceDisplayId,
      'created_by': createdBy
    };
    return feedPush(feedItem);
  }

  function paymentDeleted(companyId, invoiceId, invoiceDisplayId, paymentId, amount) {
    var feedItem = {};
    feedItem['type'] = 'PAYMENT_DELETED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = invoiceId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'payment_id': paymentId,
      'amount': amount,
      'invoice_display_id': invoiceDisplayId
    };
    return feedPush(feedItem);
  }

  function documentDeleted(companyId, documentId, documentDisplayId, documentType, deletedBy) {
    var feedItem = {};
    feedItem['type'] = 'DOCUMENT_DELETED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = documentId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'document_display_id': documentDisplayId,
      'document_type': documentType,
      'deleted_by': deletedBy
    };
    return feedPush(feedItem);
  }

  function clientCreated(companyId, clientId, clientName, createdBy) {
    var feedItem = {};
    feedItem['type'] = 'CLIENT_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = clientId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'client_name': clientName,
      'created_by': createdBy
    };
    return feedPush(feedItem);
  }

  function clientDeleted(companyId, clientId, clientName, deletedBy) {
    var feedItem = {};
    feedItem['type'] = 'CLIENT_DELETED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = clientId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'client_name': clientName,
      'deleted_by': deletedBy
    };
    return feedPush(feedItem);
  }

  function productCreated(companyId, productId, productName, createdBy) {
    var feedItem = {};
    feedItem['type'] = 'PRODUCT_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = productId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'product_name': productName,
      'created_by': createdBy
    };
    return feedPush(feedItem);
  }

  function productDeleted(companyId, productId, productName, deletedBy) {
    var feedItem = {};
    feedItem['type'] = 'PRODUCT_DELETED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = productId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'product_name': productName,
      'deleted_by': deletedBy
    };
    return feedPush(feedItem);
  }

  function inventoryAcquired(companyId, productId, productName, quantityAcquired, costPerItem, recordedBy) {
    var feedItem = {};
    feedItem['type'] = 'PRODUCT_INVENTORY_ACQUIRED';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = productId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'product_name': productName,
      'quantity_acquired': quantityAcquired,
      'cost_per_item': costPerItem,
      'recorded_by': recordedBy
    };
    return feedPush(feedItem);
  }

  function companyCreated(companyId, companyName) {
    var feedItem = {};
    feedItem['type'] = 'COMPANY_CREATED';
    feedItem['company_id'] = companyId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'name': companyName
    };
    return feedPush(feedItem);
  }

  function estimateConvertedToInvoice(companyId, invoiceId, invoiceDisplayId, convertedBy) {
    var feedItem = {};
    feedItem['type'] = 'ESTIMATE_CONVERTED_TO_INVOICE';
    feedItem['company_id'] = companyId;
    feedItem['entity_id'] = invoiceId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'converted_by': convertedBy,
      'invoice_display_id': invoiceDisplayId,
    };
    return feedPush(feedItem);
  }

  function inviteAccepted(companyId, inviteAcceptedBy) {
    var feedItem = {};
    feedItem['type'] = 'USER_ACCEPTED_INVITE';
    feedItem['company_id'] = companyId;
    feedItem['creation_epoch'] = (new Date).getTime();
    feedItem['parameters'] = {
      'accepting_user': inviteAcceptedBy
    };
    return feedPush(feedItem);
  }

  return {
    documentCreated: documentCreated,
    paymentRecorded: paymentRecorded,
    documentDeleted: documentDeleted,
    paymentDeleted: paymentDeleted,
    recurringInvoiceCreated: recurringInvoiceCreated,
    clientCreated: clientCreated,
    clientDeleted: clientDeleted,
    productCreated: productCreated,
    productDeleted: productDeleted,
    inventoryAcquired: inventoryAcquired,
    estimateConvertedToInvoice: estimateConvertedToInvoice,
    companyCreated: companyCreated,
    inviteAccepted: inviteAccepted
  };

}]);

angular.module('invoiceMakerJsLib.util').factory('fireutil', ['$firebaseAuth', '$firebaseObject', '$firebaseArray',
  function ($firebaseAuth, $firebaseObject, $firebaseArray) {
    return {
      getFirebaseStorage: function () {
        return firebase.storage().ref();
      },
      getRootFirebase: function () {
        return firebase.database().ref();
      },
      getFirebaseAuth: function () {
        return $firebaseAuth();
      },
      getSynchronizedObject: function (firebaseRef) {
        return $firebaseObject(firebaseRef);
      },
      getSynchronizedArray: function (firebaseRef) {
        return $firebaseArray(firebaseRef);
      }
    };
  }]);

angular.module('invoiceMakerJsLib.util').service('invoiceutil', [function () {
  var US_CURRENCY = {
    currency_symbol: "$",
    space_around_symbol: false,
    symbol_at_start: true,
    thousand_symbol: ",",
    decimal_symbol: ".",
    decimal_places: "2"
  };

  var UK_CURRENCY = {
    currency_symbol: "£",
    space_around_symbol: false,
    symbol_at_start: true,
    thousand_symbol: ",",
    decimal_symbol: ".",
    decimal_places: "2"
  };

  var EUROPE_CURRENCY = {
    currency_symbol: "€",
    space_around_symbol: false,
    symbol_at_start: true,
    thousand_symbol: ",",
    decimal_symbol: ".",
    decimal_places: "2"
  };

  this.suggestSubdomain = function (companyName) {
    var MAX_COMPANY_CHARS = 30;

    var legalCompanyCharacters = [];
    var code, i, len;

    for (i = 0, len = companyName.length; i < len && legalCompanyCharacters.length < MAX_COMPANY_CHARS; i++) {
      code = companyName.charCodeAt(i);
      if ((code > 47 && code < 58) || // numeric (0-9)
        (code > 64 && code < 91) || // upper alpha (A-Z)
        (code > 96 && code < 123)) { // lower alpha (a-z)
        legalCompanyCharacters.push(companyName.charAt(i).toLowerCase());
      }
    }
    return legalCompanyCharacters.join("");
  };

  this.isTaxRateValid = function (invoice, rate) {
    var invoiceRate = invoice[rate];
    return this.isTaxRateValidInternal(invoiceRate);
  };

  this.formatDateRegular = function (timezone, date_format_option, input) {
    var momentTimeZone = this.getJsTimeZone(timezone);
    var momentLocale = this.getJsLocale(date_format_option);
    var formatDate = moment.unix(input / 1000);
    return formatDate.tz(momentTimeZone).locale(momentLocale).format('LL');
  };

  this.isTaxRateValidInternal = function (invoiceRate) {
    return invoiceRate != null && invoiceRate.tax_name != null && invoiceRate.tax_rate != null && invoiceRate.tax_name.length > 0 && invoiceRate.tax_rate.length > 0;
  };

  this.calculateTaxAmount = function (item, invoice, taxRate) {
    if (!this.isTaxRateValidInternal(taxRate) || !invoice.apply_taxes) {
      return 0;
    }
    if ((item.apply_tax_1 && taxRate == invoice.tax_one) || (item.apply_tax_2 && taxRate == invoice.tax_two)) {
      var itemAmount = this.calculateRealItemRate(item) * Number(item.quantity) || 0;
      if (taxRate.compounded && taxRate == invoice.tax_two) {
        itemAmount = itemAmount + this.calculateTaxAmount(item, invoice, invoice.tax_one);
      }
      var taxRateString = taxRate.tax_rate;
      var taxRateDecimal = Number(taxRateString) / 100;
      return itemAmount * taxRateDecimal;
    } else {
      return 0;
    }
  };

  this.makePdfFilename = function (document) {
    var prefix = document.type == "INVOICE" ? "INV" : "EST";
    var displayId = document.display_id;
    return prefix + displayId + ".pdf";
  };

  this.makeSubject = function (company, document) {
    var isInvoice = document.type == "INVOICE";
    var subjectTemplateName = isInvoice ? "NEW_INVOICE" : "NEW_ESTIMATE";
    var defaultMessage = isInvoice ? "New invoice ::invoice number:: from ::company name::" : "New estimate ::estimate number:: from ::company name::";
    var subject = company.email_settings == null || company.email_settings[subjectTemplateName] == null || !company.email_settings[subjectTemplateName]
      ? defaultMessage : company.email_settings[subjectTemplateName]['subject'];
    return this.subVariables(subject, company, document);
  };

  this.subVariables = function (templateString, company, document) {
    return templateString.replace("::invoice number::", document.display_id)
      .replace("::estimate number::", document.display_id)
      .replace("::company name::", company.company_information.company_name);
  };

  this.countryCurrenciesMap = {
    "US": ["USD"],
    "CA": ["CAD", "USD"],
    "GB": ["GBP", "EUR", "USD"],
    "AU": ["AUD"],
    "BR": ["BRL"],
    "SE": ["SEK", "NOK", "DKK", "EUR", "GBP", "USD"],
    "NO": ["NOK", "DKK", "SEK", "EUR", "GBP", "USD"],
    "FI": ["EUR", "NOK", "SEK", "DKK", "GBP", "USD"],
    "IE": ["EUR", "GBP", "USD"],
    "DK": ["DKK", "NOK", "SEK", "EUR", "GBP", "USD"],
    "AT": ["EUR", "GBP", "USD"],
    "BE": ["EUR", "GBP", "USD"],
    "FR": ["EUR", "GBP", "USD"],
    "DE": ["EUR", "GBP", "USD"],
    "IT": ["EUR", "GBP", "USD"],
    "LU": ["EUR", "GBP", "USD"],
    "NL": ["EUR", "GBP", "USD"],
    "ES": ["EUR", "GBP", "USD"]
  };

  this.quarterlyDataShouldBeShown = function (quarterOne, quarterTwo, quarterThree, quarterFour) {
    function quarterHasData(singleQuarter) {
      for (var key in singleQuarter) {
        if (key == 'month_one_revenue' || key == 'month_two_revenue' || key == 'month_three_revenue') {
          if (singleQuarter.hasOwnProperty(key) && parseFloat(singleQuarter[key]) != 0) {
            return true;
          }
        }
      }
      return false;
    }

    return quarterHasData(quarterOne) || quarterHasData(quarterTwo) || quarterHasData(quarterThree) || quarterHasData(quarterFour);
  };

  this.createFullCompanyData = function (simpleSetupData, userId) {
    var returnCompany = {};
    var companyInformation = {};
    var invoiceSettings = {};
    var invoiceTemplate = {};
    returnCompany['company_information'] = companyInformation;
    returnCompany['invoice_settings'] = invoiceSettings;
    returnCompany['invoice_template'] = invoiceTemplate;

    companyInformation['company_name'] = simpleSetupData['step_one']['company_name'];
    companyInformation['contact_email'] = simpleSetupData['step_one']['company_email'];

    if ('step_two' in simpleSetupData) {
      companyInformation['address_line_one'] = simpleSetupData['step_two']['address_line_one'] || null;
      companyInformation['address_line_two'] = simpleSetupData['step_two']['address_line_two'] || null;
      companyInformation['address_line_three'] = simpleSetupData['step_two']['address_line_three'] || null;

      companyInformation['contact_fax'] = simpleSetupData['step_two']['fax'] || null;
      companyInformation['contact_phone'] = simpleSetupData['step_two']['phone'] || null;
      companyInformation['contact_business_number'] = simpleSetupData['step_two']['business_number'] || null;
    }

    var selectedCurrency = simpleSetupData['step_one']['simple_currency'];
    var currencyConfiguration;
    var timezone;
    var dateFormatOption;

    if (selectedCurrency == null || "USD" == selectedCurrency || "OTHER" == selectedCurrency) {
      currencyConfiguration = US_CURRENCY;
      dateFormatOption = "US";
      timezone = "AMERICA_NY";
    } else if ("CAD" == selectedCurrency) {
      currencyConfiguration = US_CURRENCY;
      dateFormatOption = "CANADA";
      timezone = "AMERICA_NY";
    } else if ("AUD" == selectedCurrency) {
      currencyConfiguration = US_CURRENCY;
      dateFormatOption = "AUSTRALIA";
      timezone = "AUSTRALIA_SYDNEY";
    } else if ("EUR" == selectedCurrency) {
      currencyConfiguration = EUROPE_CURRENCY;
      dateFormatOption = "UK";
      timezone = "EUROPE_BERLIN";
    } else if ("GBP" == selectedCurrency) {
      currencyConfiguration = UK_CURRENCY;
      dateFormatOption = "UK";
      timezone = "EUROPE_LONDON";
    }

    returnCompany['currency_configuration'] = currencyConfiguration;
    currencyConfiguration['timezone'] = timezone;
    currencyConfiguration['fiscal_year_start_month'] = 'JANUARY';
    invoiceSettings['date_format_setting'] = dateFormatOption;

    if ('step_three' in simpleSetupData) {
      var stepThreeData = simpleSetupData['step_three'];
      if ('logo_url' in stepThreeData) {
        invoiceTemplate['logo_url'] = stepThreeData['logo_url'];
      }
      if ('template_name' in stepThreeData) {
        invoiceTemplate['template_name'] = stepThreeData['template_name'];
      } else {
        invoiceTemplate['template_name'] = 'Modern';
      }
      if ('template_color' in stepThreeData) {
        invoiceTemplate['template_html_color'] = stepThreeData['template_color'];
      } else {
        invoiceTemplate['template_html_color'] = '#000000';
      }
    } else {
      invoiceTemplate['template_html_color'] = '#000000';
      invoiceTemplate['template_name'] = 'Modern';
    }

    invoiceSettings['next_invoice_id'] = 1;
    invoiceSettings['show_quantity_rate_columns'] = true;
    invoiceSettings['use_inventory'] = true;

    returnCompany['authorized_uids'] = {};
    returnCompany['authorized_uids'][userId] = true;
    returnCompany['creation_date'] = (new Date).getTime();
    returnCompany['owner_user_id'] = userId;

    return returnCompany;
  };

  this.displayTax = function (rate) {
    if (rate && rate.tax_name && rate.tax_rate) {
      return rate.tax_name + " (" + rate.tax_rate + "%)";
    } else {
      return "";
    }
  };

  this.displayCompanyTax = function (taxFirebaseKey, company, rateAtEpoch) {
    var tax = company.company_taxes[taxFirebaseKey];
    var currentTaxRate = this.getCurrentTaxRate(tax, rateAtEpoch);
    return tax.name + " (" + currentTaxRate.rate + "%)";
  };

  this.getCurrentTaxRate = function (tax, rateAtEpoch) {
    var winningRate = null;
    for (var key in tax.tax_rates) {
      if (tax.tax_rates.hasOwnProperty(key)) {
        var singleRate = tax.tax_rates[key];
        var effectiveEpoch = singleRate.effective_epoch;
        if (winningRate == null || (effectiveEpoch < rateAtEpoch && effectiveEpoch >= winningRate.effective_epoch)) {
          winningRate = singleRate;
        }
      }
    }
    return winningRate;
  };

  this.describeProducts = function (selectedProductsMap) {
    if (!selectedProductsMap) {
      return "";
    } else {
      var first = true;
      var returnString = "";
      for (var key in selectedProductsMap) {
        if (selectedProductsMap.hasOwnProperty(key)) {
          if (first) {
            first = false;
          } else {
            returnString = returnString + ", ";
          }
          returnString = returnString + selectedProductsMap[key].title;
        }
      }
      return returnString;
    }
  };

  this.getErrorDescriptionForError = function (error) {
    switch (error.code) {
      case "INVALID_USER":
        return "This user does not exist";
      case "EMAIL_TAKEN":
        return "Email address already in use";
      case "INVALID_PASSWORD":
        return "Incorrect password";
      case "INVALID_EMAIL":
        return "Invalid email address";
      default:
        return error.message;
    }
  };

  this.calculateRealItemRate = function (item) {
    var regularRate = item['rate'] || item['price_per_item'];
    var discountAmount = item['discount_amount'] || item['discount_per_item'];
    if (regularRate) {
      var invoiceDiscountOption = item['invoice_discount_option'] || item['discount_option'];
      var discountType = item['discount_type'];
      if (invoiceDiscountOption == 'DISCOUNT_ITEMS' && discountAmount && discountType) {
        if (discountType == 'DOLLAR') {
          return regularRate - discountAmount;
        } else {
          return regularRate - (regularRate * (discountAmount / 100));
        }
      } else {
        return regularRate;
      }
    } else {
      return 0;
    }
  };

  this.getJsTimeZone = function (invoiceTimeZone) {
    if (invoiceTimeZone == 'AMERICA_NY') {
      return 'America/New_York';
    } else if (invoiceTimeZone == 'AMERICA_CHICAGO') {
      return 'America/Chicago';
    } else if (invoiceTimeZone == 'AMERICA_PHOENIX') {
      return 'America/Phoenix';
    } else if (invoiceTimeZone == 'AMERICA_LA') {
      return 'America/Los_Angeles';
    } else if (invoiceTimeZone == 'EUROPE_LONDON') {
      return 'Europe/London';
    } else if (invoiceTimeZone == 'EUROPE_BERLIN') {
      return 'Europe/Berlin';
    } else if (invoiceTimeZone == 'EUROPE_MOSCOW') {
      return 'Europe/Moscow';
    } else if (invoiceTimeZone == 'AUSTRALIA_SYDNEY') {
      return 'Australia/Sydney';
    } else if (invoiceTimeZone == 'BRAZIL_EAST') {
      return 'Brazil/East';
    } else if (invoiceTimeZone == 'EUROPE_ROME') {
      return 'Europe/Rome';
    } else if (invoiceTimeZone == 'EUROPE_PARIS') {
      return 'Europe/Paris';
    } else if (invoiceTimeZone == 'AFRICA_JOHANNESBURG') {
      return 'Africa/Johannesburg';
    } else {
      return 'America/New_York'
    }
  };

  this.getCurrentFiscalYear = function (jsTimeZone, fiscalStartMonth) {
    var now = moment().tz(jsTimeZone);
    var currentYear = now.year();
    var currentMonth = now.month();
    var nextYear = false;

    if (!fiscalStartMonth || fiscalStartMonth == "JANUARY") {
      nextYear = false;
    } else if (fiscalStartMonth == "FEBRUARY") {
      nextYear = currentMonth >= 1;
    } else if (fiscalStartMonth == "MARCH") {
      nextYear = currentMonth >= 2;
    } else if (fiscalStartMonth == "APRIL") {
      nextYear = currentMonth >= 3;
    } else if (fiscalStartMonth == "MAY") {
      nextYear = currentMonth >= 4;
    } else if (fiscalStartMonth == "JUNE") {
      nextYear = currentMonth >= 5;
    } else if (fiscalStartMonth == "JULY") {
      nextYear = currentMonth >= 6;
    } else if (fiscalStartMonth == "AUGUST") {
      nextYear = currentMonth >= 6;
    } else if (fiscalStartMonth == "SEPTEMBER") {
      nextYear = currentMonth >= 7;
    } else if (fiscalStartMonth == "OCTOBER") {
      nextYear = currentMonth >= 8;
    } else if (fiscalStartMonth == "NOVEMBER") {
      nextYear = currentMonth >= 9;
    } else if (fiscalStartMonth == "DECEMBER") {
      nextYear = currentMonth >= 10;
    }
    return nextYear ? currentYear + 1 : currentYear;
  };

  this.getTimezoneOffset = function (invoiceTimeZone, dateEpoch) {
    return moment.unix(dateEpoch).tz(this.getJsTimeZone(invoiceTimeZone)).format('Z');
  };

  this.getJsLocale = function (invoiceLocale) {
    if (invoiceLocale == 'US') {
      return 'en';
    } else if (invoiceLocale == 'UK') {
      return 'en-gb';
    } else if (invoiceLocale == 'GERMANY') {
      return 'de';
    } else if (invoiceLocale == 'NORWAY') {
      return 'nb';
    } else if (invoiceLocale == 'ITALY') {
      return 'it';
    } else if (invoiceLocale == 'FRANCE') {
      return 'fr';
    } else if (invoiceLocale == 'JAPAN') {
      return 'ja';
    } else if (invoiceLocale == 'CHINA') {
      return 'zh-cn';
    } else if (invoiceLocale == 'CANADA') {
      return 'en-ca';
    } else if (invoiceLocale == 'AUSTRALIA') {
      return 'en-au';
    } else if (invoiceLocale == 'SOUTH_AFRICA') {
      return 'en';
    } else if (invoiceLocale == 'NETHERLANDS') {
      return 'nl';
    } else if (invoiceLocale == 'RUSSIA') {
      return 'ru';
    } else if (invoiceLocale == 'IRELAND') {
      return 'en';
    } else if (invoiceLocale == 'SPANISH') {
      return 'es';
    } else if (invoiceLocale == 'BRAZIL') {
      return 'pt-br';
    } else {
      return 'en';
    }
  }
}])
;
